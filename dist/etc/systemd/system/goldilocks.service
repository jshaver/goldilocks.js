[Unit]
Description=Goldilocks Internet Server
Documentation=https://git.daplie.com/Daplie/goldilocks.js
After=network-online.target
Wants=network-online.target systemd-networkd-wait-online.service

[Service]
# Restart on crash (bad signal), and on 'clean' failure (error exit code)
# Allow up to 3 restarts within 10 seconds
# (it's unlikely that a user or properly-running script will do this)
Restart=on-failure
StartLimitInterval=10
StartLimitBurst=3

# The v8 VM will output a "clean" for JavaScript errors.
# If we knew we were never going to accidentally exit cleanly
# we would use on-abnormal:
; Restart=on-abnormal

# User and group the process will run as
# (www-data is the de facto standard on most systems)
User=MY_USER
Group=MY_GROUP

# If we need to pass environment variables in the future
Environment=GOLDILOCKS_PATH=/srv/www NODE_PATH=/opt/goldilocks/lib/node_modules NPM_CONFIG_PREFIX=/opt/goldilocks

# Set a sane working directory, sane flags, and specify how to reload the config file
WorkingDirectory=/opt/goldilocks
ExecStart=/opt/goldilocks/bin/node /opt/goldilocks/bin/goldilocks --config /etc/goldilocks/goldilocks.yml
ExecReload=/bin/kill -USR1 $MAINPID

# Limit the number of file descriptors and processes; see `man systemd.exec` for more limit settings.
# Unmodified goldilocks is not expected to use more than this.
LimitNOFILE=1048576
LimitNPROC=64

# Use private /tmp and /var/tmp, which are discarded after goldilocks stops.
PrivateTmp=true
# Use a minimal /dev
PrivateDevices=true
# Hide /home, /root, and /run/user. Nobody will steal your SSH-keys.
ProtectHome=true
# Make /usr, /boot, /etc and possibly some more folders read-only.
ProtectSystem=full
# … except TLS/SSL, ACME, and Let's Encrypt certificates
#   and /var/log/goldilocks, because we want a place where logs can go.
#   This merely retains r/w access rights, it does not add any new. Must still be writable on the host!
ReadWriteDirectories=/etc/goldilocks /etc/ssl /srv/www /var/log/goldilocks /opt/goldilocks
# you may also want to add other directories such as /opt/goldilocks /etc/acme /etc/letsencrypt

# Note: in v231 and above ReadWritePaths has been renamed to ReadWriteDirectories
; ReadWritePaths=/etc/goldilocks /var/log/goldilocks

# The following additional security directives only work with systemd v229 or later.
# They further retrict privileges that can be gained.
# Note that you may have to add capabilities required by any plugins in use.
CapabilityBoundingSet=CAP_NET_BIND_SERVICE
AmbientCapabilities=CAP_NET_BIND_SERVICE
NoNewPrivileges=true

# Caveat: Some plugins need additional capabilities.
# For example "upload" needs CAP_LEASE
; CapabilityBoundingSet=CAP_NET_BIND_SERVICE CAP_LEASE
; AmbientCapabilities=CAP_NET_BIND_SERVICE CAP_LEASE
; NoNewPrivileges=true

[Install]
WantedBy=multi-user.target
