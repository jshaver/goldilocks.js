'use strict';

module.exports.create = function (deps, config, tcpMods) {
  var path = require('path');
  var tls = require('tls');
  var parseSni = require('sni');
  var greenlock = require('greenlock');
  var localhostCerts = require('localhost.daplie.me-certificates');
  var domainMatches = require('../domain-utils').match;

  function extractSocketProp(socket, propName) {
    // remoteAddress, remotePort... ugh... https://github.com/nodejs/node/issues/8854
    var altName = '_' + propName;
    var value = socket[propName] || socket[altName];
    try {
      value = value || socket._handle._parent.owner.stream[propName];
      value = value || socket._handle._parent.owner.stream[altName];
    } catch (e) {}

    try {
      value = value || socket._handle._parentWrap[propName];
      value = value || socket._handle._parentWrap[altName];
      value = value || socket._handle._parentWrap._handle.owner.stream[propName];
      value = value || socket._handle._parentWrap._handle.owner.stream[altName];
    } catch (e) {}

    return value || '';
  }

  function nameMatchesDomains(name, domainList) {
    return domainList.some(function (pattern) {
      return domainMatches(pattern, name);
    });
  }

  var addressNames = [
    'remoteAddress'
  , 'remotePort'
  , 'remoteFamily'
  , 'localAddress'
  , 'localPort'
  ];
  function wrapSocket(socket, opts, cb) {
    var reader = require('socket-pair').create(function (err, writer) {
      if (typeof cb === 'function') {
        process.nextTick(cb);
      }
      if (err) {
        reader.emit('error', err);
        return;
      }

      writer.write(opts.firstChunk);
      socket.pipe(writer);
      writer.pipe(socket);

      socket.on('error', function (err) {
        console.log('wrapped TLS socket error', err);
        reader.emit('error', err);
      });
      writer.on('error', function (err) {
        console.error('socket-pair writer error', err);
        // If the writer had an error the reader probably did too, and I don't think we'll
        // get much out of emitting this on the original socket, so logging is enough.
      });

      socket.on('close', writer.destroy.bind(writer));
      writer.on('close', socket.destroy.bind(socket));
    });

    // We can't set these properties the normal way because there is a getter without a setter,
    // but we can use defineProperty. We reuse the descriptor even though we will be manipulating
    // it because we will only ever set the value and we set it every time.
    var descriptor = {enumerable: true, configurable: true, writable: true};
    addressNames.forEach(function (name) {
      descriptor.value = opts[name] || extractSocketProp(socket, name);
      Object.defineProperty(reader, name, descriptor);
    });

    return reader;
  }

  var le = greenlock.create({
    server: 'https://acme-v01.api.letsencrypt.org/directory'

  , challenges: {
      'http-01': require('le-challenge-fs').create({ debug: config.debug })
    , 'tls-sni-01': require('le-challenge-sni').create({ debug: config.debug })
    , 'dns-01': deps.ddns.challenge
    }
  , challengeType: 'http-01'

  , store: require('le-store-certbot').create({
      debug: config.debug
    , configDir: path.join(require('os').homedir(), 'acme', 'etc')
    , logDir: path.join(require('os').homedir(), 'acme', 'var', 'log')
    , workDir: path.join(require('os').homedir(), 'acme', 'var', 'lib')
    })

  , approveDomains: function (opts, certs, cb) {
      // This is where you check your database and associated
      // email addresses with domains and agreements and such

      // The domains being approved for the first time are listed in opts.domains
      // Certs being renewed are listed in certs.altnames
      if (certs) {
        // TODO make sure the same options are used for renewal as for registration?
        opts.domains = certs.altnames;
        cb(null, { options: opts, certs: certs });
        return;
      }

      function complete(optsOverride, domains) {
        if (!cb) {
          console.warn('tried to complete domain approval multiple times');
          return;
        }

        // // We can't request certificates for wildcard domains, so filter any of those
        // // out of this list and put the domain that triggered this in the list if needed.
        // domains = (domains || []).filter(function (dom) { return dom[0] !== '*'; });
        // if (domains.indexOf(opts.domain) < 0) {
        //   domains.push(opts.domain);
        // }
        domains = [ opts.domain ];
        // TODO: allow user to specify options for challenges or storage.

        Object.assign(opts, optsOverride, { domains: domains, agreeTos: true });
        cb(null, { options: opts, certs: certs });
        cb = null;
      }

      var handled = false;
      if (Array.isArray(config.domains)) {
        handled = config.domains.some(function (dom) {
          if (!dom.modules || !dom.modules.tls) {
            return false;
          }
          if (!nameMatchesDomains(opts.domain, dom.names)) {
            return false;
          }

          return dom.modules.tls.some(function (mod) {
            if (mod.type !== 'acme') {
              return false;
            }
            complete(mod, dom.names);
            return true;
          });
        });
      }
      if (handled) {
        return;
      }

      if (Array.isArray(config.tls.modules)) {
        handled = config.tls.modules.some(function (mod) {
          if (mod.type !== 'acme') {
            return false;
          }
          if (!nameMatchesDomains(opts.domain, mod.domains)) {
            return false;
          }

          complete(mod, mod.domains);
          return true;
        });
      }
      if (handled) {
        return;
      }

      cb(new Error('domain is not allowed'));
    }
  });
  le.tlsOptions = le.tlsOptions || le.httpsOptions;

  var secureContexts = {};
  var terminatorOpts = require('localhost.daplie.me-certificates').merge({});
  terminatorOpts.SNICallback = function (sni, cb) {
    sni = sni.toLowerCase();
    console.log("[tlsOptions.SNICallback] SNI: '" + sni + "'");

    var tlsOptions;

    // Static Certs
    if (/\.invalid$/.test(sni)) {
      sni = 'localhost.daplie.me';
    }
    if (/.*localhost.*\.daplie\.me/.test(sni)) {
      if (!secureContexts[sni]) {
        tlsOptions = localhostCerts.mergeTlsOptions(sni, {});
        if (tlsOptions) {
          secureContexts[sni] = tls.createSecureContext(tlsOptions);
        }
      }
      if (secureContexts[sni]) {
        console.log('Got static secure context:', sni, secureContexts[sni]);
        cb(null, secureContexts[sni]);
        return;
      }
    }

    le.tlsOptions.SNICallback(sni, cb);
  };

  var terminateServer = tls.createServer(terminatorOpts, function (socket) {
    console.log('(post-terminated) tls connection, addr:', extractSocketProp(socket, 'remoteAddress'));

    tcpMods.tcpHandler(socket, {
      servername: socket.servername
    , encrypted: true
      // remoteAddress... ugh... https://github.com/nodejs/node/issues/8854
    , remoteAddress: extractSocketProp(socket, 'remoteAddress')
    , remotePort:    extractSocketProp(socket, 'remotePort')
    , remoteFamily:  extractSocketProp(socket, 'remoteFamily')
    });
  });
  terminateServer.on('error', function (err) {
    console.log('[error] TLS termination server', err);
  });

  function proxy(socket, opts, mod) {
    var newConnOpts = require('../domain-utils').separatePort(mod.address || '');
    newConnOpts.port = newConnOpts.port || mod.port;
    newConnOpts.host = newConnOpts.host || mod.host || 'localhost';
    newConnOpts.servername = opts.servername;
    newConnOpts.data = opts.firstChunk;

    newConnOpts.remoteFamily  = opts.family  || extractSocketProp(socket, 'remoteFamily');
    newConnOpts.remoteAddress = opts.address || extractSocketProp(socket, 'remoteAddress');
    newConnOpts.remotePort    = opts.port    || extractSocketProp(socket, 'remotePort');

    tcpMods.proxy(socket, newConnOpts, opts.firstChunk, function () {
      // This function is called in the event of a connection error and should decrypt
      // the socket so the proxy module can send a 502 HTTP response.
      var tlsOpts = localhostCerts.mergeTlsOptions('localhost.daplie.me', {isServer: true});
      if (opts.hyperPeek) {
        return new tls.TLSSocket(socket, tlsOpts);
      } else {
        return new tls.TLSSocket(wrapSocket(socket, opts), tlsOpts);
      }
    });
    return true;
  }

  function terminate(socket, opts) {
    console.log(
      '[tls-terminate]'
    , opts.localAddress || socket.localAddress +':'+ opts.localPort || socket.localPort
    , 'servername=' + opts.servername
    , opts.remoteAddress || socket.remoteAddress
    );

    var wrapped;
    // We can't emit the connection to the TLS server until we know the connection is fully
    // opened, otherwise it might hang open when the decrypted side is destroyed.
    // https://github.com/nodejs/node/issues/14605
    function emitSock() {
      terminateServer.emit('connection', wrapped);
    }
    if (opts.hyperPeek) {
      // This connection was peeked at using a method that doesn't interferre with the TLS
      // server's ability to handle it properly. Currently the only way this happens is
      // with tunnel connections where we have the first chunk of data before creating the
      // new connection (thus removing need to get data off the new connection).
      wrapped = socket;
      process.nextTick(emitSock);
    }
    else {
      // The hyperPeek flag wasn't set, so we had to read data off of this connection, which
      // means we can no longer use it directly in the TLS server.
      // See https://github.com/nodejs/node/issues/8752 (node's internal networking layer == 💩 sometimes)
      wrapped = wrapSocket(socket, opts, emitSock);
    }
  }

  function handleConn(socket, opts) {
    opts.servername = (parseSni(opts.firstChunk)||'').toLowerCase() || 'localhost.invalid';
    // needs to wind up in one of 2 states:
    // 1. SNI-based Proxy / Tunnel (we don't even need to put it through the tlsSocket)
    // 2. Terminated (goes on to a particular module or route, including the admin interface)
    // 3. Closed (we don't recognize the SNI servername as something we actually want to handle)

    // We always want to terminate is the SNI matches the challenge pattern, unless a client
    // on the south side has temporarily claimed a particular challenge. For the time being
    // we don't have a way for the south-side to communicate with us, so that part isn't done.
    if (domainMatches('*.acme-challenge.invalid', opts.servername)) {
      terminate(socket, opts);
      return;
    }

    if (deps.stunneld.isClientDomain(opts.servername)) {
      deps.stunneld.handleClientConn(socket);
      if (!opts.hyperPeek) {
        process.nextTick(function () {
          socket.unshift(opts.firstChunk);
        });
      }
      return;
    }

    function checkModule(mod) {
      if (mod.type === 'proxy') {
        return proxy(socket, opts, mod);
      }
      if (mod.type !== 'acme') {
        console.error('saw unknown TLS module', mod);
      }
    }

    var handled = (config.domains || []).some(function (dom) {
      if (!dom.modules || !dom.modules.tls) {
        return false;
      }
      if (!nameMatchesDomains(opts.servername, dom.names)) {
        return false;
      }

      return dom.modules.tls.some(checkModule);
    });
    if (handled) {
      return;
    }

    handled = (config.tls.modules || []).some(function (mod) {
      if (!nameMatchesDomains(opts.servername, mod.domains)) {
        return false;
      }
      return checkModule(mod);
    });
    if (handled) {
      return;
    }

    // TODO: figure out all of the domains that the other modules intend to handle, and only
    // terminate those ones, closing connections for all others.
    terminate(socket, opts);
  }

  return {
    emit: function (type, socket) {
      if (type === 'connection') {
        handleConn(socket, socket.__opts);
      }
    }
  , middleware: le.middleware()
  };
};
