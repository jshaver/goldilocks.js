safe_copy_config()
{
  src=$1
  dst=$2
  $sudo_cmd mkdir -p $(dirname "$dst")
  if [ -f "$dst" ]; then
    $sudo_cmd rsync -a "$src" "$dst.latest"
    # TODO edit config file with $my_user and $my_group
    if [ "$(cat $dst)" == "$(cat $dst.latest)" ]; then
      $sudo_cmd rm $dst.latest
    else
      echo "MANUAL INTERVENTION REQUIRED: check the systemd script update and manually decide what you want to do"
      echo "diff $dst $dst.latest"
      $sudo_cmd chown -R root:root "$dst.latest"
    fi
  else
    $sudo_cmd rsync -a --ignore-existing "$src" "$dst"
  fi
  $sudo_cmd chown -R root:root "$dst"
  $sudo_cmd chmod 644 "$dst"
}

installable=""
if [ -d "$my_root/etc/systemd/system" ]; then
  source ./installer/install-for-systemd.sh
  installable="true"
fi
if [ -d "/Library/LaunchDaemons" ]; then
  source ./installer/install-for-launchd.sh
  installable="true"
fi
if [ -z "$installable" ]; then
  echo ""
  echo "Unknown system service init type. You must install as a system service manually."
  echo '(please file a bug with the output of "uname -a")'
  echo ""
fi
