if type -p adduser >/dev/null 2>/dev/null; then
  if [ -z "$(cat $my_root/etc/passwd | grep $my_app_name)" ]; then
    $sudo_cmd adduser --home $my_root/opt/$my_app_name --gecos '' --disabled-password $my_app_name
  fi
  my_user=$my_app_name
  my_group=$my_app_name
elif [ -n "$(cat /etc/passwd | grep www-data:)" ]; then
  # Linux (Ubuntu)
  my_user=www-data
  my_group=www-data
elif [ -n "$(cat /etc/passwd | grep _www:)" ]; then
  # Mac
  my_user=_www
  my_group=_www
else
  # Unsure
  my_user=$(whoami)
  my_group=$(id -g -n)
fi
