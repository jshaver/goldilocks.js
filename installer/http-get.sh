###############################
#                             #
#         http_get            #
# boilerplate for curl / wget #
#                             #
###############################

# See https://git.daplie.com/Daplie/daplie-snippets/blob/master/bash/http-get.sh

_h_http_get=""
_h_http_opts=""
_h_http_out=""

detect_http_get()
{
  set +e
  if type -p curl >/dev/null 2>&1; then
    _h_http_get="curl"
    _h_http_opts="-fsSL"
    _h_http_out="-o"
  elif type -p wget >/dev/null 2>&1; then
    _h_http_get="wget"
    _h_http_opts="--quiet"
    _h_http_out="-O"
  else
    echo "Aborted, could not find curl or wget"
    return 7
  fi
  set -e
}

http_get()
{
  $_h_http_get $_h_http_opts $_h_http_out "$2" "$1"
  touch "$2"
}

http_bash()
{
  _http_url=$1
  #dap_args=$2
  rm -rf dap-tmp-runner.sh
  $_h_http_get $_h_http_opts $_h_http_out dap-tmp-runner.sh "$_http_url"; bash dap-tmp-runner.sh; rm dap-tmp-runner.sh
}

detect_http_get

## END HTTP_GET ##
